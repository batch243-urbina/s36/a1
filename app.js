const express = require("express");
const mongoose = require("mongoose");

const app = express();

const port = 3001;

const taskRoute = require("./routes/taskRoute");

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/tasks", taskRoute);

mongoose.connect(
  "mongodb+srv://admin:admin@zuittbatch243.m7n9nfd.mongodb.net/B243-s36-a1?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

app.listen(port, () => console.log(`Server is running at port ${port}`));
