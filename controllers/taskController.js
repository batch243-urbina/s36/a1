const Task = require("../models/task");

// Controller for create new task
module.exports.createTask = (requestBody) => {
  let newTask = new Task({
    name: requestBody.name,
  });

  return newTask.save().then((task, error) => {
    if (error) {
      console.log(error);
      return false;
    } else {
      return task;
    }
  });
};

// controller for get all tasks
module.exports.getAllTasks = () => {
  return Task.find({}).then((result) => {
    return result;
  });
};

// Controller for get specific task
module.exports.getTask = (taskId) => {
  return Task.findById(taskId).then((result, error) => {
    if (error) {
      console.log(error);
      return false;
    } else {
      return result;
    }
  });
};

// controller function for updating status
module.exports.updateStatus = (taskId, requestBody) => {
  return Task.findById(taskId).then((result, error) => {
    if (error) {
      console.log(error);
      return false;
    } else {
      result.status = requestBody.status;
      return result.save().then((updatedStatus, saveError) => {
        return saveError ? false : updatedStatus;
      });
    }
  });
};
