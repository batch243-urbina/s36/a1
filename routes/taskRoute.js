const express = require("express");
const router = express.Router();
const taskController = require("../controllers/taskController");

// POST new task route
router.post("/", (request, response) => {
  taskController
    .createTask(request.body)
    .then((resultFromController) => response.send(resultFromController));
});

// GET all task route
router.get("/", (request, response) => {
  taskController
    .getAllTasks()
    .then((resultFromController) => response.send(resultFromController));
});

// GET specific task
router.get("/:id", (request, response) => {
  taskController
    .getTask(request.params.id)
    .then((resultFromController) => response.send(resultFromController));
});

// PUT update status
router.put("/:id", (request, response) => {
  taskController
    .updateStatus(request.params.id, request.body)
    .then((resultFromController) =>
      response.send(
        `${resultFromController.name}'s status updated to ${resultFromController.status}`
      )
    );
});

// To export the router object to use in app.js
module.exports = router;
